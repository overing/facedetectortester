package org.overwork.android.fdt;

import java.nio.ByteBuffer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.FaceDetector;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class FaceDetectView extends SurfaceView implements
		SurfaceHolder.Callback, Camera.PreviewCallback, Camera.AutoFocusCallback {

	private static final String TAG = FaceDetectView.class.getSimpleName();

	private static final int MAX_FACE = 3;
	private static final int PREVIEW_WIDTH_FINE = 320;
	private static final int PREVIEW_WIDTH_NORMAL = 240;

	private Context mContext;

	private int mDisplayOrientation;

	private Handler mHandler;

	private SurfaceHolder mHolder;

	private Camera mCamera;

	private FaceDetectThread mFaceDetectThread;
	private boolean mThreadWorking;

	private OverlayLayer mOverlayLayer;

	private FaceDetector mFaceDetector;

	private boolean mLockPreview;

	private int mFdetLevel;

	private int mPrevSettingWidth, mPrevSettingHeight;
	private int mCaptureWidth, mCaptureHeight;
	private int mPreviewWidth, mPreviewHeight;

	private int mBufflen;
	private byte[] mGrayBuff;
	private int[] mRGBs;

	private boolean mTakingPicture;

	private Bitmap mBitmap_FaceMask;

	private FaceResult[] mFaces;
	
	public FaceDetectView(Context context, int displayOrientation) {
		super(context);

		mContext = context;

		mDisplayOrientation = displayOrientation;

		mHandler = new Handler();

		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		mFaces = new FaceResult[MAX_FACE];
		for (int i = 0; i < mFaces.length; i++) {
			mFaces[i] = new FaceResult();
		}

		mOverlayLayer = new OverlayLayer(mContext);
		
		mLockPreview = true;

		mTakingPicture = false;

		mBitmap_FaceMask = BitmapFactory.decodeResource(getResources(),
				android.R.drawable.ic_input_delete);
		
		mFaceDetectThread = null;
		mThreadWorking = false;
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
		setKeepScreenOn(true);

		try {
			mCamera = Camera.open();
			mCamera.setDisplayOrientation(mDisplayOrientation);
			
			mCamera.setPreviewDisplay(mHolder);
			mCamera.setPreviewCallback(this);
			mCamera.startPreview();
		} catch (Exception exception) {
			mCamera.release();
			mCamera = null;
		}

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.d(TAG, "surfaceChanged");
		
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		
		try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }
		
		resetCameraSettings();

		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.setPreviewCallback(this);
			mCamera.startPreview();
		} catch (Exception exception) {
			mCamera.release();
			mCamera = null;
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed");
		releaseCamera();
		setKeepScreenOn(false);

	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		Log.d(TAG, "onPreviewFrame");
		if (mLockPreview || mTakingPicture)
			return;
		if (data.length < mBufflen)
			return;
		// run only one analysis thread at one time
		if (!mThreadWorking) {
			mThreadWorking = true;
			// copy only Y buffer
			ByteBuffer bbuffer = ByteBuffer.wrap(data);
			bbuffer.get(mGrayBuff, 0, mBufflen);
			// make sure to wait for previous thread completes
			waitForFdetThreadComplete();
			// start thread
			mFaceDetectThread = new FaceDetectThread();
			Log.d(TAG, "start a new FaceDetectThread");
			mFaceDetectThread.start();
		}
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		// TODO Auto-generated method stub
		
	}

	public View getOverlay() {
		return mOverlayLayer;
	}

	private void releaseCamera() {
		Log.d(TAG, "releaseCamera");
		mLockPreview = true;
		mCamera.setPreviewCallback(null);
		mCamera.stopPreview();
		waitForFdetThreadComplete();
		mCamera.release();
		mCamera = null;
	}

	private void resetCameraSettings() {
		Log.d(TAG, "resetCameraSettings");
		mLockPreview = true;
		waitForFdetThreadComplete();
		// for (int i = 0; i < MAX_FACE; i++)
		// faces_[i].clear();
		/* set parameters for onPreviewFrame */
		Camera.Parameters params = mCamera.getParameters();
		// Log.i(TAG,"camera params"+params.flatten());
		String strPrevSizesVals = params.get("preview-size-values");
		String strCapSizesVals = params.get("picture-size-values");
		int previewHeightFine = 0;
		int previewHeightNorm = 0;
		if (strPrevSizesVals != null) {
			String tokens[] = strPrevSizesVals.split(",");
			for (int i = 0; i < tokens.length; i++) {
				String tokens2[] = tokens[i].split("x");
				if (tokens[i].contains(Integer.toString(PREVIEW_WIDTH_FINE)))
					previewHeightFine = Integer.parseInt(tokens2[1]);
				if (tokens[i].contains(Integer.toString(PREVIEW_WIDTH_NORMAL)))
					previewHeightNorm = Integer.parseInt(tokens2[1]);
			}
		} else {
			previewHeightFine = 240;
			previewHeightNorm = 160;
		}
		if (mFdetLevel == 0) {
			mPrevSettingWidth = PREVIEW_WIDTH_FINE;
			mPrevSettingHeight = previewHeightFine;
			// mFdtmodeBitmap = BitmapFactory.decodeResource(
			// mContext.getResources(), R.drawable.fdt_fine);
		} else {
			mPrevSettingWidth = PREVIEW_WIDTH_NORMAL;
			mPrevSettingHeight = previewHeightNorm;
			// mFdtmodeBitmap = BitmapFactory.decodeResource(
			// context_.getResources(), R.drawable.fdt_norm);
		}
		String capTokens1[] = strCapSizesVals.split(",");
		String capTokens2[] = capTokens1[capTokens1.length - 1].split("x");
		mCaptureWidth = Integer.parseInt(capTokens2[0]);
		mCaptureHeight = Integer.parseInt(capTokens2[1]);
		/*
		 * set preview size small for fast analysis. let say QQVGA by setting
		 * smaller image size, small faces will not be detected.
		 */
		if (mPrevSettingHeight != 0)
			params.setPreviewSize(mPrevSettingWidth, mPrevSettingHeight);
		if (mCaptureWidth != 0)
			params.setPictureSize(mCaptureWidth, mCaptureHeight);
		// params.setPreviewFrameRate(5);
		mCamera.setParameters(params);
		/* setParameters do not work well */
		params = mCamera.getParameters();
		Camera.Size size = params.getPreviewSize();
		mPreviewWidth = size.width;
		mPreviewHeight = size.height;
		Log.i(TAG, "reset preview size, w:" + mPreviewWidth + ",h:" + mPreviewHeight);
		size = params.getPictureSize();
		Log.i(TAG, "reset picture size, w:" + size.width + ",h:" + size.height);
		// allocate work memory for analysis
		mBufflen = mPreviewWidth * mPreviewHeight;
		mGrayBuff = new byte[mBufflen];
		mRGBs = new int[mBufflen];
		float aspect = (float) mPreviewHeight / (float) mPreviewWidth;
		mFaceDetector = new FaceDetector(mPrevSettingWidth,
				(int) (mPrevSettingWidth * aspect), MAX_FACE);
		mLockPreview = false;
	}

	private void waitForFdetThreadComplete() {
		Log.d(TAG, "waitForFdetThreadComplete");
		if (mFaceDetectThread == null)
			return;
		if (mFaceDetectThread.isAlive()) {
			try {
				mFaceDetectThread.join();
				Log.i(TAG, "thread deleted.");
				mFaceDetectThread = null;
			} catch (InterruptedException e) {
				Log.w(TAG, e);
			}
		}
	}

	private class OverlayLayer extends View {

		private Paint mPaint;

		public OverlayLayer(Context context) {
			super(context);
			mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			mPaint.setStyle(Paint.Style.STROKE);
			mPaint.setColor(0xFF33FF33);
			mPaint.setStrokeWidth(3);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			Log.d(TAG, "OverlayLayer.onDraw");
			super.onDraw(canvas);

			int w = canvas.getWidth();
			int h = canvas.getHeight();
			float xRatio = (float) w / mPreviewWidth;
			float yRatio = (float) h / mPreviewHeight;
			for (int i = 0; i < MAX_FACE; i++) {
				FaceResult face = mFaces[i];
				float eyedist = face.eyesDistance() * xRatio;
				if (eyedist == 0.0f)
					continue;
				PointF midEyes = new PointF();
				face.getMidPoint(midEyes);

				PointF lt = new PointF(midEyes.x * xRatio - eyedist * 1.75f,
						midEyes.y * yRatio - eyedist * 1.75f);
				canvas.drawBitmap(mBitmap_FaceMask, null, new Rect((int) lt.x,
						(int) lt.y, (int) (lt.x + eyedist * 3.5f),
						(int) (lt.y + eyedist * 3.5f)), mPaint);
			}
		}
	}

	private class FaceDetectThread extends Thread {

		@Override
		public void run() {
			Log.d(TAG, "FaceDetectThread.run");

			// grayToRPG(mGrayBuff, mRGBs);
			gray8toRGB32(mGrayBuff, mPreviewWidth, mPreviewHeight, mRGBs);

			float aspect = (float) mPreviewHeight / (float) mPreviewWidth;
			int w = mPrevSettingWidth;
			int h = (int) (mPrevSettingWidth * aspect);
			float xScale = (float) mPreviewWidth / (float) mPrevSettingWidth;
			float yScale = (float) mPreviewHeight / (float) mPrevSettingHeight;
			Bitmap bmp = Bitmap.createScaledBitmap(Bitmap.createBitmap(mRGBs,
					mPreviewWidth, mPreviewHeight, Bitmap.Config.RGB_565), w,
					h, false);
			// Log.i(TAG,"downscale w="+bmp.getWidth()+",h="+bmp.getHeight());
			int prevfound = 0, trackfound = 0;
			for (int i = 0; i < MAX_FACE; i++) {
				FaceResult face = mFaces[i];
				float eyedist = face.eyesDistance();
				if (eyedist == 0.0f)
					continue;
				PointF midEyes = new PointF();
				face.getMidPoint(midEyes);
				prevfound++;
				PointF lt = new PointF(midEyes.x - eyedist * 2.5f, midEyes.y
						- eyedist * 2.5f);
				Rect rect = new Rect((int) (lt.x), (int) (lt.y),
						(int) (lt.x + eyedist * 5.0f),
						(int) (lt.y + eyedist * 5.0f));
				/* fix to fit */
				rect.left = rect.left < 0 ? 0 : rect.left;
				rect.right = rect.right > w ? w : rect.right;
				rect.top = rect.top < 0 ? 0 : rect.top;
				rect.bottom = rect.bottom > h ? h : rect.bottom;
				if (rect.left >= rect.right || rect.top >= rect.bottom)
					continue;
				/* crop */
				Bitmap facebmp = Bitmap.createBitmap(bmp, rect.left, rect.top,
						rect.width(), rect.height());
				FaceDetector.Face[] trackface = new FaceDetector.Face[1];
				FaceDetector tracker = new FaceDetector(facebmp.getWidth(),
						facebmp.getHeight(), 1);
				int found = tracker.findFaces(facebmp, trackface);
				if (found != 0) {
					PointF ptTrack = new PointF();
					trackface[0].getMidPoint(ptTrack);
					ptTrack.x += (float) rect.left;
					ptTrack.y += (float) rect.top;
					ptTrack.x *= xScale;
					ptTrack.y *= yScale;
					float trkEyedist = trackface[0].eyesDistance() * xScale;
					mFaces[i].setFace(ptTrack, trkEyedist);
					trackfound++;
				}
			}
			if (prevfound == 0 || prevfound != trackfound) {
				FaceDetector.Face[] fullResults = new FaceDetector.Face[MAX_FACE];
				mFaceDetector.findFaces(bmp, fullResults);
				/* copy result */
				for (int i = 0; i < MAX_FACE; i++) {
					if (fullResults[i] == null)
						mFaces[i].clear();
					else {
						PointF mid = new PointF();
						fullResults[i].getMidPoint(mid);
						mid.x *= xScale;
						mid.y *= yScale;
						float eyedist = fullResults[i].eyesDistance() * xScale;
						mFaces[i].setFace(mid, eyedist);
					}
				}
			}

			/* post message to UI */
			mHandler.post(new Runnable() {
				public void run() {
					mOverlayLayer.postInvalidate();
					// turn off thread lock
					mThreadWorking = false;
				}
			});
		}

		private void gray8toRGB32(byte[] gray8, int width, int height,
				int[] rgb_32s) {
			final int endPtr = width * height;
			int ptr = 0;
			while (true) {
				if (ptr == endPtr)
					break;
				final int Y = gray8[ptr] & 0xff;
				rgb_32s[ptr] = 0xff000000 + (Y << 16) + (Y << 8) + Y;
				ptr++;
			}
		}
	}

	private class FaceResult extends Object {
		private PointF mMidEye;
		private float mEyeDist;

		public FaceResult() {
			mMidEye = new PointF(0.0f, 0.0f);
			mEyeDist = 0.0f;
		}

		public void setFace(PointF midEye, float eyeDist) {
			set(midEye, eyeDist);
		}

		public void clear() {
			set(new PointF(0.0f, 0.0f), 0.0f);
		}

		private synchronized void set(PointF midEye, float eyeDist) {
			mMidEye.set(midEye);
			mEyeDist = eyeDist;
		}

		public float eyesDistance() {
			return mEyeDist;
		}

		public void getMidPoint(PointF pt) {
			pt.set(mMidEye);
		}

	}

}
