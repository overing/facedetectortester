package org.overwork.android.fdt;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.ViewGroup;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();

	private FaceDetectView mFaceDetectView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);

		mFaceDetectView = new FaceDetectView(this, getDisplayOrientation());
		setContentView(mFaceDetectView);

		final int wc = ViewGroup.LayoutParams.WRAP_CONTENT;
		addContentView(mFaceDetectView.getOverlay(),
				new ViewGroup.LayoutParams(wc, wc));

	}

	private int getDisplayOrientation() {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(0, info);
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}
		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}

}
